# Converting svn to git keeping full branch history

Properly converting svn repository to git is not easy task as you might think at first glance.
The core of the problem is that svn-branches are just directories in the svn-root.
You can easily import trunk branch to git. But if you want to keep changes in all branches, you will have problems.
If you import svn-root to git, you will get set of svn-branch dirs in the git repo, and it is most likely not what you need.
Moreover if developers have switched trunk from one branch to another, each such switch will be filed as one big diff, that add the mess to the code-change history.
In this article I'd try to tell how I solved this problem: convert messy svn repo into git repo with clean commit history.

The repository I've imported, is old svn repo of libtcod library, we wanted to keep available for historical purposes.
The archive of the repo [have been provided](https://github.com/libtcod/libtcod/issues/119#issuecomment-1197401532) by retired library developer, and I've deployed it at `svn://svn.nataraj.su/libtcod-svn-repo` (Hope all these links will be available for a while)

## Understanding how commit chains should go

First thing you need to do is to build actual commit tree, to see how commits are split to branches.
In svn one revision may belong to several branches: if you have some old branch, and forked it to a new one, all commits of old branch will also be considered to be commits of a new branch.
So, to reproduce development workflow in git, can't just commit all svn commits of a branch to the git branch.
You need to understand how branches have been forked, and follow that commit-flow while importing commits.

I've written a script that will put all branch history into `.csv` file that can be manually colored in LibreOffice Calc, and there you can carefully explore commit history.
To do this you should checkout whole svn repo with all branches. For my repo it should be

```
svn checkout svn://svn.nataraj.su/libtcod-svn-repo/
```

Go to the repo dir, and write commit logs of all branches to files, like that:

```
cd branches/1.5.0; svn log > ../1.5.0.log
```
do that for each branch of the project.

Then take `parse_rev_history.pl` from this repo, put it in the folder with log files, add all your branch names in the `my @branches = qw( trunk 1.4 1.5 1.5.0);` list, and run the script.
You should get content of `.csv` file printed to the `STDOUT`.
Redirect output to the file, load it with LibreOffice Calc.
If you manually add colored marks to you branch names you might get something like that:

![spreadsheet_example.png](spreadsheet_example.png)

Here you can see, there was development of 1.4 branch, and of a trunk.
In rev. 96 trunk have been forked to 1.5, later branch 1.5 has only one commit of its own, and then development went on in the trunk (than will be forked to 1.5.0 later).
Interesting thing here, is that in rev. 98 trunk was overwritten with content of 1.4 branch (a huge diff in that commit).
So trunk should be considered abandoned in rev. 97 and freshly forked from 1.4 in rev 98.
It can't be seen from this spreadsheet, but this is how commit-chains should go in git repo.

## How import single revision to git

There is only one bullet proof way to import one commit of the commit chain from the svn to git:

* `svn checkout` next revision in the svn repo;
* `git rm -f .` delete all files from the git repо;
* copy all files from svn repo to git repo
* `git add .` in the git repo, to put all files back to the version control.

These steps allow you to add commit exactly as it should be with all file deletions etc.

In `create_git.pl` `apply_rev` function do the thing.
It also parses commit log for proper commit date/time, proper committer, etc, and add all this info to the git.

## Adding commit chains as git branches

First you use your commit-flow spreadsheet, we've created before, to get revision numbers in commit chain you going to import (use sorting to do that).
Use copy-paste, and put them into `create_git.pl` to a list like that:

```
my @branch1_4_revs = qw(1 3 4 6 8 16 23 24 26 27 29 31 36 42 45 46 48 50 54 55 56 57 58 59 61 62 64 66 68 69 70 72 73 74 78 79 80 81 82 83 84 85 86 88 89 90 91 92 93 94 95 100 );
```

Then, when you, while importing commits, reach the point where branch should start, you just do

```
print `cd new_git; git checkout -b svn_1.4`;
```

and then iterate over `@branch1_4_revs` adding commits one by one

```
foreach my $rev (@branch1_4_revs)
{
  my $svn_branch = $rev_branches[$rev] || "branches/1.4";
  apply_rev("new_git", $rev, $svn_branch);

  if ($rev == 95) # last commit to 1.4 branch before split
  {
    print `cd new_git; git branch svn_1.5`;
  }
}
```

As you can see, this loop has if condition, that adds a branch at certain point.
You can later check out that branch, and iterate over `@branch1_5_revs` list adding commits to it.

Actually you should completely rewrite `create_git.pl` so it created branches and committed commit-chains exactly as it is done in your original svn-repo.

This would take a while, and require some efforts, but finally you should get perfect result.

## Miscellaneous

### Adding committer names and mails

As you can notice, svn uses only committer login as committer ID.
Git is using e-mail and full name for it.
You can add both committer names and e-mails, if you know them, to `%committer_names` and `%committer_mails` hashes in your `create_git.pl`.
This data will be used, while creating commits in git repo.

### Problem with CRLF (Dos) line feeds

Some developers do not pay attention to using (or not using) CRLF (Dos) line feeds.
This might create total mess in the commit history.
In my case, in rev.98 a copy of 1.4 branch have been placed back in the trunk, but line feeds was replaced in many files, so it created a terrible total diff, spoiled all `git blame`, etc.

To straighten things up, I forced git to completely ignore CRLFs and always replace them with LFs during commit:

```
print `cd new_git; git config --local core.autocrlf input`;
```

It is not an option I would suggest to everyone, if in original svn line feeds are well managed, you should keep them the way they are.
But in my case it was not an option.

## Afterwards

This text and script does not provide ready to use solution that allow to import any svn repository to git.
But it provide some tools and tips that will allow to create custom convert solution that fits your repository.
At least I will save it here, so I can use it in the future. May be I hope it will be helpful to somebody else.
