#!/usr/bin/perl

use strict;
use Path::Tiny;

my @branches = qw( trunk 1.4 1.5 1.5.0);

my $res = {};
foreach my $branch (@branches)
{
  my $file = path("$branch.log")->slurp;

  foreach my $item (split /---------+\n/, $file)
  {
    $item =~ /^r(\d+)/;
    my $rev = $1;

    my @lines = split "\n", $item;

    $res->{$rev} ||= {branches => {}, text=> $lines[2] };
    $res->{$rev}->{branches}->{$branch} = 1;
  }
}

foreach my $rev (sort {$a <=> $b} keys %$res)
{
  print "$rev\t";
  foreach my $branch (@branches)
  {
    print $branch if $res->{$rev}->{branches}->{$branch};
    print "\t";
  }
  print $res->{$rev}->{text};
  print "\n";
}

