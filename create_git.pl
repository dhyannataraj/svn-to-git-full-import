#!/usr/bin/perl

use strict;

my @branch1_4_revs = qw(1 3 4 6 8 16 23 24 26 27 29 31 36 42 45 46 48 50 54 55 56 57 58 59 61 62 64 66 68 69 70 72 73 74 78 79 80 81 82 83 84 85 86 88 89 90 91 92 93 94 95 100 );

my @branch1_5_deprecated_revs = qw(5 7 9 10 11 12 13 14 15 17 18 19 20 21 22 25 28 30 32 33 34 35 37 38 39 40 41 43 44 47 49 51 52 53 60 63 65 67 71 76 77 87 96 99 218);

my @branch1_5_revs = qw(

98 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122 123 124 125 126 127 128 129 130 131 132 133 134 135 136 137 138 139 140 141 142 143 144 145 146 147 148 149 150 
151 152 153 154 155 156 157 158 159 160 161 162 163 164 165 166 167 168 169 170 171 172 173 174 175 176 177 178 179 180 181 182 183 184 185 186 187 188 189 190 191 192 193 194 195 196 197 198 199 200 201
 202 203 204 205 206 207 208 209 210 211 212 213 214 215 216 217 219 496 497
);

my @trunk_revs = qw(
220 221 222 223 224 225 226 227 228 229 230 231 232 233 234 235 236 237 238 239 240 241 242 243 244 245 246 247 248 249 250 251 252 253 254 255 256 257 258 259 260 261 262 263 264 265 266 267 268 269
270 271 272 273 274 275 276 277 278 279 280 281 282 283 284 285 286 287 288 289 290 291 292 293 294 295 296 297 298 299 300 301 302 303 304 305 306 307 308 309 310 311 312 313 314 315 316 317 318 319
320 321 322 323 324 325 326 327 328 329 330 331 332 333 334 335 336 337 338 339 340 341 342 343 344 345 346 347 348 349 350 351 352 353 354 355 356 357 358 359 360 361 362 363 364 365 366 367 368 369
370 371 372 373 374 375 376 377 378 379 380 381 382 383 384 385 386 387 388 389 390 391 392 393 394 395 396 397 398 399 400 401 402 403 404 405 406 407 408 409 410 411 412 413 414 415 416 417 418 419
420 421 422 423 424 425 426 427 428 429 430 431 432 433 434 435 436 437 438 439 440 441 442 443 444 445 446 447 448 449 450 451 452 453 454 455 456 457 458 459 460 461 462 463 464 465 466 467 468 469
470 471 472 473 474 475 476 477 478 479 480 481 482 483 484 485 486 487 488 489 490 491 492 493 494 495 498 499 500 501 502 503 504 505 506 507 508 509 510 511 512 513 514 515 516 517 518 519 520 521
522 523 524 525 537 538 539 540 541 542
);



my @rev_branches;
$rev_branches[1] = "trunk";

my %committer_names = (
mingos => 'Dominik Marczuk',
donblas => 'Chris "donblas" Hamons',
jotaf => 'Jotaf',
);

my %committer_mails = (
jice => 'jice.nospam@gmail.com',
mingos => 'mingos.nospam@gmail.com',
donblas => 'chris.hamons@microsoft.com',
dividee => 'ibatugow@gmail.com',
jotaf => 'jotaf98@hotmail.com',
joeosborn => 'joseph.osborn@pomona.edu',
);

print `rm -rf new_git`;
print `mkdir new_git`;

print `git init new_git`;
#print `cd new_git; git config --local core.autocrlf input`;

my $git_dir = "new_git";

print `touch new_git/.empty_file`;
print `cd new_git; git add .; GIT_COMMITTER_DATE='Jan 05  00:00:01 2008' GIT_COMMITTER_NAME='none' GIT_COMMITTER_EMAIL='none' git commit -a -m"First empty commit" --date='Jan 05  00:00:01 2008'  --author='none <none>'`;
=cut
print `cd $git_dir; git rm -r .  >/dev/null`;
print `cp -r 001/.  $git_dir `;
print `cd $git_dir; git add .    >/dev/null`;
print `cd new_git; git add .; GIT_COMMITTER_DATE='Mon Jul 14 00:00:00 2008 +0400' GIT_COMMITTER_NAME='none' GIT_COMMITTER_EMAIL='none' git commit -a -m"Import revision from 1.3.2 release archive" --date='Mon Jul 14 00:00:00 2008 +0400'  --author='none <none>'`;

print `cd $git_dir; git rm -r .  >/dev/null`;
print `cp -r 002/.  $git_dir `;
print `cd $git_dir; git add .    >/dev/null`;
print `cd new_git; git add .; GIT_COMMITTER_DATE='Thu Oct 16 00:00:00 2008 +0400' GIT_COMMITTER_NAME='none' GIT_COMMITTER_EMAIL='none' git commit -a -m"Import revision from 1.4.0 release archive" --date='Thu Oct 16 00:00:00 2008 +0400'  --author='none <none>'`;

print `cd $git_dir; git rm -r .  >/dev/null`;
print `cp -r 003/.  $git_dir `;
print `cd $git_dir; git add .    >/dev/null`;
print `cd new_git; git add .; GIT_COMMITTER_DATE='Thu Oct 16 00:00:01 2008 +0400' GIT_COMMITTER_NAME='none' GIT_COMMITTER_EMAIL='none' git commit -a -m"Import revision from 1.4.0 release archive, addendum" --date='Thu Oct 16 00:00:01 2008 +0400'  --author='none <none>'`;
=cut

print `cd new_git; git checkout -b svn_1.5_deprecated`;
print `cd new_git; git checkout -b svn_1.4`;



foreach my $rev (@branch1_4_revs)
{
  my $svn_branch = $rev_branches[$rev] || "branches/1.4";
  apply_rev("new_git", $rev, $svn_branch);

  if ($rev == 95) # last commit to 1.4 branch before split
  {
    print `cd new_git; git branch svn_1.5`;
  }
}


print `cd new_git; git checkout svn_1.5_deprecated`;

foreach my $rev (@branch1_5_deprecated_revs)
{
  my $svn_branch = $rev_branches[$rev] || "branches/1.5-deprecated";
  apply_rev("new_git", $rev, $svn_branch);
}


print `cd new_git; git checkout svn_1.5`;

foreach my $rev (@branch1_5_revs)
{
  my $svn_branch = $rev_branches[$rev] || "branches/1.5.0";
  apply_rev("new_git", $rev, $svn_branch);
  if ($rev == 219) # tag 1.5.0 version
  {
    print `cd new_git; git checkout -b svn_1.5.0`;
  }
}

print `cd new_git; git checkout svn_1.5`;

foreach my $rev (@trunk_revs)
{
  my $svn_branch = $rev_branches[$rev] || "trunk";
  apply_rev("new_git", $rev, $svn_branch);
}


sub apply_rev
{
  my $git_dir = shift;
  my $svn_rev = shift;
  my $svn_branch = shift;

  print "====== $svn_rev ======\n";
  print `rm -rf svn_wrk`;
  print `mkdir svn_wrk`;


  print `svn checkout -r $svn_rev svn://svn.nataraj.su/libtcod-svn-repo/$svn_branch svn_wrk  #   >/dev/null`;

  my $log = `cd svn_wrk; LANG=C svn log `;
  $log =~ s/^-+\n//s;
  $log =~ /^r(\d+)\s+\|\s+(.*?)\s+\|\s+(.*?)\s+\|/; # r1 | jice | 2009-06-28 15:12:12 +0400 (Sun, 28 Jun 2009) | 1 line
  my $committer = $2;
  my $date_raw = $3;

  $date_raw =~/(\d+:\d+:\d+\s+.*?)\s/;
  my $time = $1;
  $date_raw =~/\((.*)\)/;
  my $commit_date = $1;
  $commit_date =~ s/,//; # remove comma

  $commit_date .= " $time";

  my $committer_name = $committer_names{$committer} || $committer;
  my $committer_mail = $committer_mails{$committer} || die;

  $log =~s/^.*?\n//s;  # remove first line
  $log =~ /^(.*?)\n------------------------------------------------------------------------\n/s;

  my $commit_message = $1;

  $commit_message = '[Empty Commit Message]' if $commit_message=~ /^\s*$/;

  $commit_message =~ s{\\}{\\\\}gs;
  $commit_message =~ s/\"/\\\"/gs;
  $commit_message =~ s/\$/\\\$/gs;

  print `rm -rf svn_wrk/.svn      >/dev/null`;
  print `cd $git_dir; git rm -r .  >/dev/null`;
  print `cp -r svn_wrk/.  $git_dir `;
  print `cd $git_dir; git add .    >/dev/null`;

  print `cd $git_dir; GIT_COMMITTER_DATE='$commit_date' GIT_COMMITTER_NAME='$committer_name' GIT_COMMITTER_EMAIL='$committer_mail' git commit --date='$commit_date'  --author='$committer_name <$committer_mail>' -m "$commit_message"   >/dev/null`;
}
